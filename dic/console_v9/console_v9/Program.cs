﻿using System;
using System.Collections.Generic;
using Lib_v9;

namespace console_v9
{
    class Program
    {
        static void Main(string[] args)
        {
            //Конструктор с параметрами
            Console.WriteLine("1. Демонстрация конструктора с параметром. Параметры: файл TestFile.txt");
            Dictionary myDictionary = new Dictionary("TestFile.txt");
            Console.WriteLine("Содержимое файла:");
            myDictionary.Show();
            Console.WriteLine();


            //Конструктор без параметров и Insert
            Console.WriteLine("2. Демонстрация работы метода Insert");
            myDictionary = new Dictionary();
            myDictionary.Insert("язык", "language");
            myDictionary.Show();
            Console.WriteLine();

            //Конструктор без параметров и Delete
            Console.WriteLine("3. Демонстрация работы метода Delete");
            myDictionary = new Dictionary();
            myDictionary.Insert("язык", "language");
            myDictionary.Insert("учитель", "teacher");
            Console.WriteLine("Начальный словарь:");
            myDictionary.Show();
            Console.WriteLine("Удаляем 'язык'");
            myDictionary.Delete("язык");
            Console.WriteLine("Содержимое словаря:");
            myDictionary.Show();
            Console.WriteLine();
            

            //Метод Translate
            Console.WriteLine("4. Демонстрация работы метода Translate");
            myDictionary = new Dictionary();
            myDictionary.Insert("язык", "language");
            myDictionary.Insert("учитель", "teacher");
            Console.WriteLine("Начальный словарь:");
            myDictionary.Show();
            string translate = "учитель язык";
            Console.WriteLine("Строка для перевода: {0}", translate);
            Console.WriteLine("Перевод: {0}", myDictionary.Translate(translate));
            Console.WriteLine();

            //Метод Unique
            Console.WriteLine("5. Демонстрация работы метода Unique");
            myDictionary = new Dictionary();
            myDictionary.Insert("язык", "language");
            myDictionary.Insert("учитель", "teacher");
            myDictionary.Insert("язык", "speech");
            Console.WriteLine("Начальный словарь:");
            myDictionary.Show();
            Console.WriteLine("Содержимое результата метода Unique");
            HashSet<string> result = myDictionary.Uniquie();
            foreach (string s in result)
            {
                Console.WriteLine(s);
            }

            //Метод NumLen1
            Console.WriteLine("6. Демонстрация работы метода NumLen1");
            myDictionary = new Dictionary();
            myDictionary.Insert("язык", "language");
            myDictionary.Insert("ключ", "key");
            Console.WriteLine("Начальный словарь:");
            myDictionary.Show();
            Console.WriteLine("NumLen1: {0}",myDictionary.NumLen1());
            Console.ReadKey();
        }
    }
}

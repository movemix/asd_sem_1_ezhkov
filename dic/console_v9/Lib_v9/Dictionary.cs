﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Lib_v9
{
    /// <summary>
    /// Пользовательский класс "Словарь"
    /// </summary>
    public class Dictionary
    {
        //первый элемент в словаре
        private DicNode _dicNode;

        //Конструктор класса считывающий содержимое словаря из файла
        public Dictionary(string fileName)
        {
            //если файл существует...
            if (File.Exists(fileName))
            {
                //идём по каждой строке файла
                foreach (string line in File.ReadAllLines(fileName))
                {
                    //разделяем сроки на слова
                    string[] str = line.Split(null);
                    //если в строке два слова
                    if (str.Length == 2)
                    {
                        //вставляем "значение" и "перевод"
                        Insert(str[0], str[1]);
                    }
                }
            }
        }

        //пустой конструктор
        public Dictionary()
        {
        }

        //метод выводящий содержимое словаря в понятной для пользователя форме (на консоль)
        public void Show()
        {
            //цикл по каждому элементу словаря
            DicNode current = _dicNode;
            while (current != null)
            {
                Console.WriteLine("Слово: {0}; Перевод: {1}", current.Source, current.Translate);
                current = current.Next;
            }
        }

        //Метод добавляющий элемент словаря
        public void Insert(string k, string v)
        {
            //создаем новый узел
            DicNode newDicNode = new DicNode
            {
                Source = k,
                Translate = v
            };

            //если словарь не пустой....
            if (_dicNode != null)
            {
                //переходим к последнему узлу
                DicNode lastDicNode = _dicNode;
                while (lastDicNode.Next != null)
                {
                    lastDicNode = _dicNode.Next;
                }

                //добавляем ссылку последнего узла на новый элемент
                lastDicNode.Next = newDicNode;
                //добавляем ссылку "предыдущий" у нового на последний элемент
                newDicNode.Prev = lastDicNode;
            }
            else
            {
                //если список пуст - новый элемент становиться первым
                _dicNode = newDicNode;
            }

        }

        //Метод удаляющий слово из словаря
        public void Delete(string k)
        {
            DicNode currentDicNode = _dicNode;

            do
            {
                var prev = currentDicNode.Prev;
                var next = currentDicNode.Next;

                //если текущий узел содержит удаляемое слово
                if (currentDicNode.Source.CompareTo(k) == 0)
                {
                    //для исключения элемента из цепочки изменим ссылки предыдущего и последущего по отношению к нему
                    //исключив текущий элемент из ссылок мы удалим его
                    if (prev != null)
                    {
                        prev.Next = currentDicNode.Next;
                    }

                   
                    if (next != null)
                    {
                        next.Prev = prev;
                    }
                }

                currentDicNode = currentDicNode.Next;
            }
            while (currentDicNode != null);
            _dicNode = currentDicNode;
        }

        //Метод возвращающий уникальные слова (без дублей)
        public HashSet<string> Uniquie()
        {
            //результат нахождения уникальных слов
            HashSet<string> result = new HashSet<string>();

            //идем по каждому узлу списка
            DicNode current = _dicNode;
            while(current!=null)
            {
                string source = current.Source;
                int count=0;
                //переходим к первому элементу в списке
                DicNode nextDicNode = current;
                while (nextDicNode.Prev != null)
                {
                    nextDicNode = nextDicNode.Prev;
                }

                //идем по всем элементам списка и считаем количество искомого слова
                while (nextDicNode != null)
                {
                    if (nextDicNode.Source.CompareTo(source) == 0)
                    {
                        count++;
                        //если количество больше 1 - то дальне нет смысла искать (это слово нам не подоходит)
                        if (count > 1)
                        {
                            break;
                        }
                    }
                    nextDicNode = nextDicNode.Next;
                }

                //если в результате поиска искомое слово встретилось один раз - добавляем его в результирующий набор
                if (count==1)
                {
                    result.Add(source);
                }

                current = current.Next;
            }

            return result;
        }

        //Метод возвращает слова, перевод которых отличается по длине только на 1 знак
        public int NumLen1()
        {
            int result = 0;
            //проходим по всем списку и проверяем отличие длин слова от его перевода
            DicNode current = _dicNode;
            while (current != null)
            {
                //считаем только те слова, модуль от разницы длин переводов которых равен 1
                if (Math.Abs(current.Source.Length - current.Translate.Length) == 1)
                {
                    result++;
                }

                current = current.Next;
            }

           return result;
        }

        //Метод возвращает перевод строки
        public string Translate(string text)
        {
            //разбиваем строку на слова
            string[] source = text.Split(null);

            //сюда будет собираться результат перевода
            var sb = new StringBuilder();
            
            //идем по каждому слову
            foreach (string s in source)
            {
                //ищем перевод
                DicNode current = _dicNode;
                while (current != null)
                {
                    //если нашли перевод - добавляем его в результат
                    if (current.Source.CompareTo(s) == 0)
                    {
                        sb.Append(current.Translate);
                        sb.Append(' ');
                    }
                    current = current.Next;
                }
            }

            //выводим результат перевода (без конечных пробелов)
            var result = sb.ToString().Trim();
            return result;
        }
    }
}

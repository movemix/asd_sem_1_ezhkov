﻿namespace Lib_v9
{
    /// <summary>
    /// Узел содержащий перевод одного слова. Представляет собой двунаправленный список
    /// </summary>
    internal class DicNode
    {
        public DicNode Prev; //ссылка на предыдущий элемент списка
        public DicNode Next; //ссылка на следующий элемент списка
        public string Source; //исходное слово
        public string Translate; //перевод исходного слова
    }
}

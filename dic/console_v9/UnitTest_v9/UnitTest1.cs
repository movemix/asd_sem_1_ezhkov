﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lib_v9;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest_v9
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void InsertMethodTest()
        {
            var myDictionary = new Dictionary();
            myDictionary.Insert("мир", "word");
            Assert.AreEqual("word", myDictionary.Translate("мир"));
        }

        [TestMethod]
        public void DeleteMethodTest()
        {
            var myDictionary = new Dictionary();
            myDictionary.Insert("мир", "word");
            myDictionary.Delete("мир");
            Assert.AreEqual(string.Empty, myDictionary.Translate("мир"));
        }

        [TestMethod]
        public void UniqueMethodTest()
        {
            var myDictionary = new Dictionary();
            myDictionary.Insert("мир", "word");
            myDictionary.Insert("привет", "hi");
            myDictionary.Insert("привет", "hello");

            HashSet<string> result = new HashSet<string> { "мир" };
            Assert.AreEqual(result.First(), myDictionary.Uniquie().First());
            Assert.AreEqual(1, myDictionary.Uniquie().Count);
        }

        [TestMethod]
        public void NumLen1Test()
        {
            var myDictionary = new Dictionary();
            myDictionary.Insert("мир", "word");
            myDictionary.Insert("привет", "hi");
            Assert.AreEqual(1, myDictionary.NumLen1());
        }

        [TestMethod]
        public void ConstructorWithParameterTest()
        {
            var myDictionary = new Dictionary("TestFile.txt");
        }

        [TestMethod]
        public void ShowTest()
        {
            var myDictionary = new Dictionary();
            myDictionary.Show();
        }
    }
}
